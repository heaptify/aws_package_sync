
LAMBDAS += sqs_to_dynamodb

AWS_REGION := us-east-1
S3_BUCKET := packages-eu-west-1
S3_PREFIX := lambda/aws-package-manager
AWS_PROFILE := test

SHELL := /bin/bash
ROOT_DIR := "$(CURDIR)"

all: build-all package-all

test:
	echo $(AWS_PROFILE)

_set_lambda_vars:
	$(eval LAMBDA_NAME=$(shell grep ^Resources -A1 $(ROOT_DIR)/lambda/$(dirname)/template.yaml | tail -n 1 | cut -d: -f 1 | awk '{print $$1}'))

build-all:
	@for dirname in $(LAMBDAS); do \
		$(MAKE) build-lambda dirname=$$dirname || exit 1; \
	done

build-lambda: _set_lambda_vars
	@if [ ! -e $(ROOT_DIR)/lambda/$(dirname)/$(LAMBDA_NAME).zip ]; then \
		cd $(ROOT_DIR)/lambda/$(dirname) && pipenv lock -r > src/requirements.txt ; \
		cd $(ROOT_DIR)/lambda/$(dirname) && sam build ; \
	fi

package-all:
	@for dirname in $(LAMBDAS); do \
		$(MAKE) package-lambda dirname=$$dirname || exit 1; \
	done

package-lambda: build-lambda
	@echo "Packaging $(dirname) into $(LAMBDA_NAME).zip and creating checksum $(LAMBDA_NAME).zip.chksum"
#	cd lambda/$(dirname) && sam package --s3-bucket $(S3_BUCKET) --s3-prefix $(S3_PREFIX) --profile $(AWS_PROFILE) --region $(AWS_REGION) --debug
	@cd $(ROOT_DIR)/lambda/$(dirname)/.aws-sam/build/$(LAMBDA_NAME) && \
		zip -rq9 $(ROOT_DIR)/lambda/$(dirname)/$(LAMBDA_NAME).zip * && \
		sha256sum $(ROOT_DIR)/lambda/$(dirname)/$(LAMBDA_NAME).zip | cut -f1 -d\ | sed 's/\([0-9A-Fa-f]\{2\}\)/\\\\\\x\1/g' | xargs printf | base64 > $(ROOT_DIR)/lambda/$(dirname)/$(LAMBDA_NAME).zip.chksum; \

test-all:
	@for dirname in $(LAMBDAS); do \
		$(MAKE) test-lambda dirname=$$dirname || exit 1; \
	done

test-lambda:
	@echo "Testing $(dirname)"
	@cd $(ROOT_DIR)/lambda/$(dirname) && \
		direnv exec . pytest -x -v

cover-lambda:
	@echo "Checking unit test coverage for $(dirname)"
	@cd $(ROOT_DIR)/lambda/$(dirname) && \
		direnv exec . pytest --cov-report term-missing --cov=src --cov-fail-under=20  --cov-report html

cover-all:
	@for dirname in $(LAMBDAS); do \
		$(MAKE) cover-lambda dirname=$$dirname || exit 1; \
	done

upload-all: clean-all
	@for dirname in $(LAMBDAS); do \
		$(MAKE) upload-lambda dirname=$$dirname || exit 1; \
	done

upload-lambda: package-lambda
	@echo "Uploading $(LAMBDA_NAME).zip"
	@aws s3 cp $(ROOT_DIR)/lambda/$(dirname)/$(LAMBDA_NAME).zip s3://$(S3_BUCKET)/$(S3_PREFIX)/$(LAMBDA_NAME).zip \
		--profile $(AWS_PROFILE)

clean-all:
	@for dirname in $(LAMBDAS); do \
		$(MAKE) clean-lambda dirname=$$dirname || exit 1; \
	done

clean-lambda: _set_lambda_vars
	@echo "Cleaning $(dirname)"
	@rm -rf $(ROOT_DIR)/lambda/$(dirname)/.aws-sam \
		$(ROOT_DIR)/lambda/$(dirname)/$(LAMBDA_NAME).zip \
		$(ROOT_DIR)/lambda/$(dirname)/$(LAMBDA_NAME).zip.chksum
	@rm -rf $(ROOT_DIR)/lambda/$(dirname)/htmlcov

wipe-all:
	@for dirname in $(LAMBDAS); do \
		$(MAKE) wipe-lambda dirname=$$dirname || exit 1; \
	done

wipe-lambda: _set_lambda_vars clean-lambda
	@echo "Wiping $(dirname)"
	@rm -rf $(ROOT_DIR)/lambda/$(dirname)/.venv

run-lambda:
	@if [ -z "$(dirname)" ]; then echo "Usage: make run-lambda dirname=package_renewal_scan"; exit 1; fi
	@if [ -f "$(ROOT_DIR)/lambda/$(dirname)/tests/events/event_for_kinesis.json" ]; then \
		echo "Will create a kinesis event file (/tmp/kinesis_event.json) from event_for_kinesis.json"; \
		$(ROOT_DIR)/tools/json_to_kinesis_record.py -i "$(ROOT_DIR)/lambda/$(dirname)/tests/events/event_for_kinesis.json" > /tmp/kinesis_event.json; \
		cd $(ROOT_DIR)/lambda/$(dirname) && sam local invoke --event /tmp/kinesis_event.json --docker-network aws; \
		rm /tmp/kinesis_event.json; \
	else \
		cd $(ROOT_DIR)/lambda/$(dirname) && sam local invoke --event tests/events/event.json --docker-network aws; \
    fi

provision-dev:
	@cd $(ROOT_DIR)/provision && terraform apply -var-file=testenv.tfvars

provision-validate:
	@cd $(ROOT_DIR)/provision && terraform validate

direnv-allow-all:
	for envrcdir in `find . -name .envrc -type f | xargs -I{} dirname {}`; do \
		echo "$$envrcdir" && \
		direnv allow $(ROOT_DIR)/$$envrcdir/.envrc && \
		cd $(ROOT_DIR)/$$envrcdir; \
	done

python-setup-all:
	@for dirname in $(LAMBDAS); do \
		$(MAKE) python-setup-lambda dirname=$$dirname || exit 1; \
	done

python-setup-lambda:
	@echo "Installing Python dependecies for $(dirname)"
	@direnv allow $(ROOT_DIR)/lambda/$(dirname)/.envrc
	@cd $(ROOT_DIR)/lambda/$(dirname) && direnv exec . pipenv install --dev

run-script-lambda: _set_lambda_vars
	@if [ -z "$(dirname)" || -z "$(scriptname)"  ]; then echo "Usage: make run-lambda dirname=package_renewal_scan scriptname=delete_table.sh"; exit 1; fi
	@cd $(ROOT_DIR)/lambda/$(dirname) && ./tests/bin/$(scriptname)

setup_dev_env:
	@cd $(ROOT_DIR) && \
	direnv exec . direnv allow

lambda_dirs := $(foreach lambda_dir, $(shell find lambda -mindepth 1 -maxdepth 1 -type d | sed 's,lambda/,,' | sort ), $(lambda_dir))
missing_lambdas := $(strip $(foreach lambda, $(lambda_dirs),$(if $(findstring $(lambda),$(LAMBDAS)),,$(lambda))))

dynamodb-list-local:
	@aws dynamodb list-tables --endpoint-url ${LOCALSTACK_ENDPOINT_URL} --output json

dynamodb-delete-all-local:
	@aws dynamodb delete-table --endpoint-url ${LOCALSTACK_ENDPOINT_URL}  --table-name package-all --output json

dynamodb-create-all-local:
	@./lambda/sqs_to_dynamodb/tests/bin/create_package_table.sh
