from abc import ABC, abstractmethod
from collections import deque
from collections import Counter
from typing import Any, List


class ProcessAndPersist(ABC):
    """A Base class for data processing and persisting"""

    def __init__(
        self,
        process_batch_size: int = 1,
        persist_batch_size: int = 1,
    ):
        self._process_batch_size = process_batch_size
        self._persist_batch_size = persist_batch_size
        self._process_queue = deque()
        self._persist_queue = deque()
        self._output_failure_queue = []

        # unset
        self._run_completed = None

    @abstractmethod
    def _process(self):
        """This method should:
        - consume N items from self._process_queue (N=process_batch_size)
        - put processed messages into self._persist_queue
        """
        pass

    @abstractmethod
    def _persist(self):
        """This method should:
        - consume M items from self._persist_queue (M=persist_batch_size)
        - put result in self._output_queue (if necessary)
        """
        pass

    def _process_condition(self):
        """
        Function to decide whether to run _process()
        """
        return True if self._process_queue else False

    def _persist_condition(self):
        """
        Function to decide whether to run _persist()
        """
        return True if self._persist_queue else False

    def _queue_items_for_processing(self, items: List[Any]):
        """
        Function to create process queue
        """
        self._process_queue.extend(items)

    def run(self, item_list: List[Any]):
        """
        Entry point for process and persist.
        It returns a tuple of (success_deque, failure_deque, stats).
        """
        self._output_queue = []
        self._stats = Counter()
        self._queue_items_for_processing(item_list)
        while self._process_condition() or self._persist_condition():
            if self._persist_condition():
                self._persist_pre_work()
                self._persist()
                self._persist_post_work()
            else:
                self._process_pre_work()
                self._process()
                self._process_post_work()
        self._run_final_work()
        return (self._output_queue, self._output_failure_queue, self._stats)

    @staticmethod
    def deque_popleft_batch(d: deque, n: int) -> List[Any]:
        """Get a batch of size=n from left of a deque as a list"""
        batch = []
        try:
            for _ in range(n):
                batch.append(d.popleft())
        except IndexError:
            pass
        finally:
            return batch

    def _process_pre_work(self):
        """
        Function to run before each _process()
        """
        pass

    def _persist_pre_work(self):
        """
        Function to run before each _persist()
        """
        pass

    def _process_post_work(self):
        """
        Function to run after each _process()
        """
        pass

    def _persist_post_work(self):
        """
        Function to run after each _persist()
        """
        pass

    def _run_final_work(self):
        """
        Function to run final works before run() returns
        """
        pass

    @property
    def run_completed(self):
        """
        Attribute to indicate if the process queue has been exhausted
        """
        return self._run_completed

    @run_completed.setter
    def run_completed(self, value: bool):
        self._run_completed = value
