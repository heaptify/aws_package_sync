import logging
import os

import boto3
from fleece.log import setup_root_logger, get_logger

from misc import get_event_records, get_log_level_from_env
from sqstodynamodb import SQSToDynamoDB

# logs
setup_root_logger()
log_level = get_log_level_from_env("LOG_LEVEL", logging.INFO)
cloudwatch_logger = get_logger(name="CloudWatch", level=log_level)

# globals
TABLE_NAME = "package-all"

# boto resources and clients
dynamodb = boto3.resource("dynamodb")
cloudwatch = boto3.client("cloudwatch")


# test mode
if os.environ.get("LOCAL_TEST", "") == "1":
    print("LOCAL TEST MODE")
    dynamodb = boto3.resource("dynamodb", endpoint_url="http://localstack:4569")

dynamodb_table = dynamodb.Table(TABLE_NAME)


def main(event, context):
    """
    This function consumes SQS messages and persists them in dynamodb table.
    """
    try:
        records = get_event_records(event)
        cloudwatch_logger.debug(message="Records received", records=records)
        ddb_persister = SQSToDynamoDB(
            dynamodb_table=dynamodb_table, logger=cloudwatch_logger
        )

        (_, _, stats) = ddb_persister.run(records)

    except Exception as e:
        cloudwatch_logger.error(
            message="Problem encountered processing and persisting SQS messages",
            exc_info=e,
        )
        raise
    else:
        cloudwatch_logger.info(
            message="SQS records consumed successfully",
            record_count=len(records),
            remaining_time=context.get_remaining_time_in_millis(),
            stats=stats,
        )
        return {
            "message": "SQS records consumed",
            "record_count": len(records),
            "stats": stats,
        }
