import json
import logging
import os
from typing import Dict, List


def get_log_level_from_env(
    env_var_name: str = "LOG_LEVEL", default_log_level: int = logging.INFO
) -> int:
    """
    Gets log level from environment variable 'env_var_name'.
    Returns 'default_log_level' if 'env_var_name' if not defined or is invalid.
    Supported log levels are: CRITICAL, ERROR, WARNING, INFO and DEBUG.
    Log levels are case-insensitive.
    """
    name_to_log_level = {
        "CRITICAL": logging.CRITICAL,
        "ERROR": logging.ERROR,
        "WARNING": logging.WARNING,
        "INFO": logging.INFO,
        "DEBUG": logging.DEBUG,
    }
    log_level_env = os.environ.get(env_var_name, "").upper()
    log_level = name_to_log_level.get(log_level_env, default_log_level)
    return log_level


def get_sqs_records(event_records) -> List[Dict]:
    """Returns a list of records from an SQS event records"""
    records = []
    for msg in event_records:
        record = json.loads(msg["body"])
        records.append(record)
    return records


def get_event_records(event, *args, **kwargs) -> List[Dict[str, str]]:
    """
    Returns records from event. It supports sqs and plain json dump records.
    It assumes an event has records from a single source.
    """
    records = []
    if "Records" in event and event["Records"]:
        if isinstance(event["Records"], list):
            event_records = event["Records"]
        elif isinstance(event["Records"], dict):
            event_records = [event["Records"]]
        if event_records:
            event_source = event_records[0].get("eventSource", "json")
            if event_source.endswith("sqs"):
                records = get_sqs_records(event_records)
            else:
                records = event_records
    return records
