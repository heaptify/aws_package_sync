from logging import Logger
import re
from typing import Dict, Tuple

import arrow

from processandpersisttodynamodb import ProcessAndPersistToDynamoDB

CloudWatchClient = "botocore.client.CloudWatch"
DynamoDBTable = "boto3.resources.factory.dynamodb.Table"

DATETIME_FORMAT = "YYYY-MM-DD HH:mm:ss"
NULL_STR = "\\N"
IGNORED_K = "IgnoredRecords"
ITEM_HASH_K = "ItemHash"
ITEM_RANGE_K = "ItemRange"
INELIGIBLE_RECORDS_K = "IneligibleRecords"
RECORD_TIME_K = "RecordTime"
PERSISTED_K = "PersistedRecords"
PROCESSED_K = "ProcessedRecords"
PROCESSING_ERROR_K = "ProcessingErrors"
DMS_DATA_K = "data"
DMS_METADATA_K = "metadata"
DMS_META_TABLENAME_K = "table-name"
DMS_META_OPERATION_K = "operation"
DMS_META_OPERATION_INSERT_K = "insert"
DMS_META_OPERATION_UPDATE_K = "update"
DMS_META_RECORDTYPE_K = "record-type"
DMS_META_SCHEMANAME_K = "schema-name"

DATE_TIME_MIN_LEN = len(DATETIME_FORMAT)


class SQSToDynamoDB(ProcessAndPersistToDynamoDB):
    """
    A class to keep sync package changes
    """

    DDB_HASH = ITEM_HASH_K
    DDB_SORT = ITEM_RANGE_K
    DATABASE = "test"
    TABLE_KEYS = {
        # TableName : ((HashKeys), SortKey)
        "Package": (("ID",), "Activated"),
        "MNO": ((None,), "ID"),
    }

    ITEM_KEYS = TABLE_KEYS

    def __init__(
        self,
        dynamodb_table: DynamoDBTable,
        logger: Logger,
    ):
        self._key_list = [ITEM_HASH_K, ITEM_RANGE_K]
        super().__init__(
            dynamodb_table=dynamodb_table,
            key_list=self._key_list,
            logger=logger,
            add_record_time=True,
        )
        # for date/time in this format: 2020-12-31T16:30:45
        self._reg_date_time = re.compile(r"^\d\d\d\d\-\d\d\-\d\dT\d\d:\d\d:\d\d")

    def _get_keys(self, table_name: str, item: Dict[str, str]) -> Tuple[str, str, str]:
        """Returns (HashKey, RangeKey) for the item"""

        (hash_keys, sort_key_name) = self.ITEM_KEYS[table_name]
        hash_key_list = [
            f"{k}:{item.get(k, NULL_STR)}" for k in hash_keys if k is not None
        ]

        hash_key_list_str = "|".join(hash_key_list)
        if hash_key_list_str:
            hash_key = "{}|{}".format(table_name, hash_key_list_str)
        else:
            hash_key = table_name
        sort_key = "{}:{}".format(sort_key_name, item[sort_key_name])
        return (hash_key, sort_key)

    def _check_dms_metafilter(self, metadata: Dict, metafilter: Dict) -> bool:
        """Checks whether metadata matches metafilter"""
        if metafilter:
            for k, v in metafilter.items():
                if isinstance(v, (list, tuple)):
                    if metadata[k] not in v:
                        return False
                else:
                    if k not in metadata or metadata[k] != v:
                        return False
        return True

    def _dms_parse_event(self, event: Dict, metafilter: Dict) -> Tuple[Dict, Dict]:
        """
        Returns a (record, metadata) tuple from event based on metafilter.
        If 'event' doesn't have 'data' and 'metadata' keys, it will return (None, None)
        """
        try:
            data = event[DMS_DATA_K]
            metadata = event[DMS_METADATA_K]
        except KeyError:
            return None, None

        if not self._check_dms_metafilter(metadata, metafilter):
            return None, metadata

        record = {}
        for k, v in data.items():
            # for null
            # v can be integer 0 so we can't do: "if not v:"
            if not v and v != 0:
                v_str = NULL_STR
            # for SET MySQL type which comes as a list
            # TODO: Do we receive lists from DMS for SET types?
            elif isinstance(v, list):
                v_str = ",".join(v)
            else:
                v_str = str(v)
            try:
                v_str = self._change_datetime_format(v_str)
            except ValueError as e:
                self._logger.error(
                    message="Problem with date/time conversion.",
                    attr=v_str,
                    data=data,
                    exp_info=e,
                )
            record[k] = v_str
        return record, metadata

    def _change_datetime_format(self, value: str):
        """
        DMS sends date in this format: '2020-01-30T12:30:45Z'
        However, we need date in this format: '2020-01-30 12:30:45'
        This function takes care of that.
        If the 'value' is in '2020-01-30T12:30:45Z' format , it will convert it to
        '2020-01-30 12:30:45' and return. Otherwise it will return the same 'value'.
        """
        value_str = str(value)
        if (
            len(value_str) >= DATE_TIME_MIN_LEN
            and value_str[10] == "T"
            and self._reg_date_time.match(value_str)
        ):
            # convert time to UTC just in case it is in another TZ
            arr_date = arrow.get(value_str).to("utc")
            return arr_date.format(DATETIME_FORMAT)
        return value

    def _process(self):
        """Generates DynamoDB records from Kinesis stream records"""
        record_batch = self.deque_popleft_batch(
            self._process_queue, self._process_batch_size
        )
        for dms_record in record_batch:
            self.log_debug(message="Processing record", record=dms_record)
            try:
                record, metadata = self._dms_parse_event(
                    event=dms_record,
                    metafilter={
                        DMS_META_OPERATION_K: (
                            DMS_META_OPERATION_INSERT_K,
                            DMS_META_OPERATION_UPDATE_K,
                        ),
                        DMS_META_RECORDTYPE_K: DMS_DATA_K,
                        DMS_META_SCHEMANAME_K: self.DATABASE,
                    },
                )

                if not record:
                    self._stats[IGNORED_K] += 1
                    self._logger.info(
                        message="Ignoring record",
                        data=dms_record,
                    )
                    continue

                table_name = metadata[DMS_META_TABLENAME_K]

                self._stats[table_name] += 1
                (hash_key, sort_key) = self._get_keys(table_name, record)

                record[self.DDB_HASH] = hash_key
                record[self.DDB_SORT] = sort_key

                self._add_record_to_persist_queue(record)

            except KeyError as e:
                self._stats[PROCESSING_ERROR_K] += 1
                self._logger.error(
                    message="Key Error in record",
                    record=dms_record,
                    exc_info=e,
                )
            except Exception as e:
                self._stats[PROCESSING_ERROR_K] += 1
                self._logger.error(
                    message="Error in processing record",
                    record=dms_record,
                    exc_info=e,
                )
        self._logger.info(
            message="Processed records from batch",
            batch_size=len(record_batch),
            ignored_records=self._stats[IGNORED_K],
            processed_records=self._stats[PROCESSED_K],
            processing_errors=self._stats[PROCESSING_ERROR_K],
        )
