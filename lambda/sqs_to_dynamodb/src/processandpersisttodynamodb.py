from logging import Logger
from typing import Dict, List, Optional

import arrow
import boto3


from processandpersist import ProcessAndPersist

CloudWatchClient = "botocore.client.CloudWatch"
DynamoDBTable = "boto3.resources.factory.dynamodb.Table"

DATETIME_FORMAT_HIGHRES = "YYYY-MM-DD HH:mm:ss.SSSSSS"
INELIGIBLE_RECORDS_K = "IneligibleRecords"
RECORD_TIME_K = "RecordTime"
PERSISTED_K = "PersistedRecords"
PROCESSED_K = "ProcessedRecords"
PROCESSING_ERROR_K = "ProcessingErrors"


class ProcessAndPersistToDynamoDB(ProcessAndPersist):
    """
    A class to process records and persist them in a DynamoDB table in bulk.
    It doesn't have any explicit retry mechanism as it uses boto3 batch_writer()
    which has it's own retry and error handling mechanism.
    """

    PROCESS_BATCH_SIZE = 100
    PERSIST_BATCH_SIZE = 25

    def __init__(
        self,
        dynamodb_table: DynamoDBTable,
        key_list: List[str],
        logger: Logger,
        add_record_time: bool = False,
    ) -> None:
        super().__init__(
            process_batch_size=self.PROCESS_BATCH_SIZE,
            persist_batch_size=self.PERSIST_BATCH_SIZE,
        )
        self._dynamodb_table = dynamodb_table
        self._key_list = key_list
        self._logger = logger
        if self._logger:
            self.log_debug = self._logger.debug
            self.log_info = self._logger.info
            self.log_error = self._logger.error
        self._add_record_time = add_record_time

    def _check_record(self, record: Dict[str, str]) -> bool:
        """
        Check whether record is eligible to put in or not
        Return True if eligible else False
        This can be overwritten by child classes
        """
        if record:
            return True
        return False

    def _transform_record(self, record: Dict[str, str]) -> Dict[str, str]:
        """
        Transforms a record and returns it
        This can be overwritten by child classes
        """
        return record

    def _add_record_to_persist_queue(self, record: Dict[str, str]) -> None:
        """Adds item to persist queue"""
        if self._add_record_time:
            record[RECORD_TIME_K] = arrow.utcnow().format(DATETIME_FORMAT_HIGHRES)
        self._persist_queue.append(record)
        self._stats[PROCESSED_K] += 1
        self._logger.debug(
            message="Adding record to persist queue",
            record=record,
        )

    def _process(self) -> None:
        """
        For each record in records, call:
            _check_record()
            _transform_record()
            add them to persist queue
        """
        batch = self.deque_popleft_batch(self._process_queue, self._process_batch_size)
        for record in batch:
            try:
                self._logger.debug(
                    message="Processing record",
                    record=record,
                )
                if self._check_record(record):
                    transformed_record = self._transform_record(record)
                    self._add_record_to_persist_queue(transformed_record)
                else:
                    self._stats[INELIGIBLE_RECORDS_K] += 1
            except Exception as e:
                self._stats[PROCESSING_ERROR_K] += 1
                self._logger.error(
                    message="Exception in processing record",
                    record=record,
                    exc_info=e,
                )
        self._logger.info(
            message="Processed records", record_count=len(batch), stats=self._stats
        )

    def _persist(self) -> None:
        """
        Persists records in DynamoDB
        Raises an Exception or returns stats if persisted succesfully
        """
        batch = self.deque_popleft_batch(self._persist_queue, self._persist_batch_size)
        try:
            with self._dynamodb_table.batch_writer(
                overwrite_by_pkeys=self._key_list
            ) as ddb_writer:
                for item in batch:
                    ddb_writer.put_item(Item=item)
                    self._logger.debug(message="Persisting item in DynamoDB", item=item)
        except Exception as e:
            self._logger.error(
                message="Error in ProcessAndPersistToDynamoDB",
                batch=batch,
                exc_info=e,
            )
            raise
        else:
            self._logger.info(
                message="Success in ProcessAndPersistToDynamoDB",
                item_count=len(batch),
            )
            self._stats[PERSISTED_K] += len(batch)
            self._output_queue.extend(batch)
            return self._stats
