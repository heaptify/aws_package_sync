#!/bin/bash
aws dynamodb create-table\
 --endpoint-url $LOCALSTACK_ENDPOINT_URL\
 --table-name package-all\
 --attribute-definitions\
   AttributeName=ItemHash,AttributeType=S\
   AttributeName=ItemRange,AttributeType=S\
 --key-schema\
   AttributeName=ItemHash,KeyType=HASH\
   AttributeName=ItemRange,KeyType=RANGE\
 --provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=10\
 --output json
