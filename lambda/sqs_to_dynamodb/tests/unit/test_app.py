from unittest.mock import MagicMock

import pytest
from moto import mock_dynamodb2

from app import main


@pytest.fixture
def event():
    event = {"Records": []}
    return event


@pytest.fixture
def context():
    context = MagicMock()
    context.get_remaining_time_in_millis = MagicMock(return_val=123456)
    return context


@mock_dynamodb2
def test_main_empty_event(event, context):
    """Make sure lambda is able to run with an empty event"""
    response = main(event, context)
    assert response["record_count"] == 0
