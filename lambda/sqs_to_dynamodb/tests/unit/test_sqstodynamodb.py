import logging
from typing import Any, Dict, List

import boto3
from fleece.log import setup_root_logger, get_logger
import pytest
from moto import mock_dynamodb2, mock_cloudwatch

from sqstodynamodb import SQSToDynamoDB

setup_root_logger()
cloudwatch_logger = get_logger(name="CloudWatch", level=logging.INFO)

DDBTABLE = "package-all"


@pytest.fixture
def dynamodb_table():
    """Returns a mock DynamoDB UDR_HISTORY_TABLE Table"""
    with mock_dynamodb2():
        dynamodb = boto3.resource("dynamodb")
        dynamodb.create_table(
            TableName=DDBTABLE,
            KeySchema=[
                {"AttributeName": "ItemHash", "KeyType": "HASH"},
                {"AttributeName": "ItemRange", "KeyType": "RANGE"},
            ],
            AttributeDefinitions=[
                {"AttributeName": "ItemHash", "AttributeType": "S"},
                {"AttributeName": "ItemRange", "AttributeType": "S"},
            ],
            ProvisionedThroughput={"ReadCapacityUnits": 50, "WriteCapacityUnits": 50},
        )
        dynamodb_table = dynamodb.Table(DDBTABLE)
        yield dynamodb_table


def get_sqstodynamodb(ddb_table):
    with mock_cloudwatch():
        sqstodynamodb = SQSToDynamoDB(
            dynamodb_table=ddb_table,
            logger=cloudwatch_logger,
        )
        return sqstodynamodb


def run_and_compare(records, expected_items, ddb_table):
    """
    Run PackageSyncer with records and then check whether expected_items exist
    in ddb_table
    """
    package_syncer = get_sqstodynamodb(ddb_table)
    (success_list, failure_list, stats) = package_syncer.run(records)

    assert len(success_list) == len(expected_items)
    assert len(failure_list) == 0

    for expected_item in expected_items:
        hash_key = expected_item["ItemHash"]
        range_key = expected_item["ItemRange"]
        response = ddb_table.get_item(
            Key={"ItemHash": hash_key, "ItemRange": range_key}
        )
        assert "Item" in response
        item = response["Item"]
        for k in expected_item:
            if k not in item:
                print("MISSING KEY: ", k)
            assert k in item
            if item[k] != expected_item[k]:
                print(
                    "DIFFERENCE IN KEY: {} -> {} vs {}".format(
                        k, item[k], expected_item[k]
                    )
                )
            assert item[k] == expected_item[k]


def get_ddb_stream_record_for_table(
    record: Dict[str, str], table_name: str
) -> Dict[str, Any]:
    """Returns a DynamoDB stream type record for record and table_name"""
    ddb_stream_record = {
        "data": record,
        "metadata": {
            "timestamp": "2020-09-30T10: 00: 44.316222Z",
            "record-type": "data",
            "operation": "insert",
            "partition-key-type": "schema-table",
            "schema-name": "tigrillo",
            "table-name": table_name,
            "transaction-id": 8224883005967,
        },
    }
    return ddb_stream_record


def get_ddb_stream_record_list_for_table(
    record_list: List[Dict[str, str]], table_name: str
) -> List[Dict[str, Any]]:
    """Returns a DynamoDB stream type record list for record_list and table_name"""
    ddb_stream_record_list = [
        get_ddb_stream_record_for_table(r, table_name) for r in record_list
    ]
    return ddb_stream_record_list


def test_packagesync_Package(dynamodb_table):
    """Test whether we insert Package records properly in DynamoDB"""
    records = [
        {
            "data": {
                "ID": "8090",
                "Activated": "2013-09-09 15:11:24",
                "MNOID": "894453",
                "Title": "10MB/Month Zone 1 (SMS+VPN+CSD) Pooled",
                "Description": "10MB/Month Zone 1  (SMS+VPN+CSD) ONLY Data Number Pooled - no longer activate only pre-provision on package",  # noqa: E501
                "PackageCurrencyID": "GBP",
                "SIMPrice": "3.00",
                "NoChargePeriods": "0",
                "Price": "8.00",
                "Period": "1",
                "MinNoOfPeriods": "12",
                "BillingDayOfMonth": "1",
                "PackageCategoryID": "5",
                "OutOfService": "\\N",
                "State": "CR8D",
                "DefaultMCC": "234",
                "AutoProvision": "no",
                "Flags": "\\N",
                "CreatedDate": "2016-01-21T11:48:07Z",
                "ModifiedDate": "2018-10-18T09:42:57Z",
            },
            "metadata": {
                "timestamp": "2020-09-30T10: 00: 44.316222Z",
                "record-type": "data",
                "operation": "insert",
                "partition-key-type": "schema-table",
                "schema-name": "tigrillo",
                "table-name": "Package",
                "transaction-id": 8224883005967,
            },
        }
    ]

    expected_items = [
        {
            "ItemHash": "Package|ID:8090",
            "ItemRange": "Activated:2013-09-09 15:11:24",
            "ID": "8090",
            "Activated": "2013-09-09 15:11:24",
            "Title": "10MB/Month Zone 1 (SMS+VPN+CSD) Pooled",
            "Description": "10MB/Month Zone 1  (SMS+VPN+CSD) ONLY Data Number Pooled - no longer activate only pre-provision on package",  # noqa: E501
            "PackageCurrencyID": "GBP",
            "SIMPrice": "3.00",
            "NoChargePeriods": "0",
            "Price": "8.00",
            "Period": "1",
            "MinNoOfPeriods": "12",
            "BillingDayOfMonth": "1",
            "PackageCategoryID": "5",
            "OutOfService": "\\N",
            "State": "CR8D",
            "DefaultMCC": "234",
            "AutoProvision": "no",
            "Flags": "\\N",
            "CreatedDate": "2016-01-21 11:48:07",
            "ModifiedDate": "2018-10-18 09:42:57",
        },
    ]

    run_and_compare(records, expected_items, dynamodb_table)


def test_packagesync_MNO(dynamodb_table):
    """Test whether we insert MNO records properly in DynamoDB"""
    records = [
        {
            "data": {
                "ID": "89254",
                "MCC": "639",
                "MNC": "02",
                "CreatedDate": "2012-01-01 00:00:00",
                "ModifiedDate": "2020-08-24 22:09:43",
                "StartDate": "2012-01-01 00:00:00",
                "OutOfService": "\\N",
                "State": "ATPN",
                "Datacentre": "the,ken",
                "TADIG": "\\N",
            },
            "metadata": {
                "timestamp": "2020-09-30T10: 00: 44.316222Z",
                "record-type": "data",
                "operation": "insert",
                "partition-key-type": "schema-table",
                "schema-name": "tigrillo",
                "table-name": "MNO",
                "transaction-id": 8224883005967,
            },
        }
    ]

    expected_items = [
        {
            "ItemHash": "MNO",
            "ItemRange": "ID:89254",
            "ID": "89254",
            "MCC": "639",
            "MNC": "02",
            "CreatedDate": "2012-01-01 00:00:00",
            "ModifiedDate": "2020-08-24 22:09:43",
            "StartDate": "2012-01-01 00:00:00",
            "OutOfService": "\\N",
            "State": "ATPN",
            "Datacentre": "the,ken",
            "TADIG": "\\N",
        },
    ]

    run_and_compare(records, expected_items, dynamodb_table)


@pytest.mark.parametrize(
    "metadata, metafilter, expected_result",
    [
        (
            {
                "record-type": "data",
                "operation": "insert",
                "schema-name": "tigrillo",
                "table-name": "Package",
            },
            {
                "operation": (
                    "insert",
                    "update",
                ),
                "record-type": "data",
                "schema-name": "tigrillo",
            },
            True,
        ),
        ({"operation": "update"}, {"operation": "insert"}, False),  # different op
        ({"operation": "update"}, {"schema-name": "tigrillo"}, False),  # missing op
        (
            {
                "operation": "insert",
                "schema-name": "tigrillo",
                "table-name": "PackageItem",
            },
            {
                "operation": ("insert"),
                "schema-name": "tigrillo",
                "table-name": "Package",
            },
            False,
        ),
    ],
)
def test_check_dms_metafilter(dynamodb_table, metadata, metafilter, expected_result):
    """Test _check_dms_metafilter()"""
    package_syncer = get_sqstodynamodb(dynamodb_table)
    assert package_syncer._check_dms_metafilter(metadata, metafilter) == expected_result


@pytest.mark.parametrize(
    "dms_record, metafilter, expected_record, expected_metadata",
    [
        (
            {  # dms_record
                "data": {"ID": 1, "Price": 15.375, "Title": "UK Package"},
                "metadata": {
                    "record-type": "data",
                    "operation": "insert",
                    "partition-key-type": "schema-table",
                    "schema-name": "tigrillo",
                },
            },
            {  # metafilter
                "operation": (
                    "insert",
                    "update",
                ),
                "record-type": "data",
                "schema-name": "tigrillo",
            },
            {"ID": 1, "Price": 15.375, "Title": "UK Package"},  # record
            {  # metadata
                "record-type": "data",
                "operation": "insert",
                "partition-key-type": "schema-table",
                "schema-name": "tigrillo",
            },
        ),
        (
            {  # dms_record, no 'data'
                "metadata": {
                    "record-type": "data",
                    "operation": "insert",
                    "partition-key-type": "schema-table",
                    "schema-name": "tigrillo",
                },
            },
            {  # metafilter
                "operation": (
                    "insert",
                    "update",
                ),
                "record-type": "data",
                "schema-name": "tigrillo",
            },
            None,  # record
            None,  # metadata
        ),
        (
            {  # dms_record, no 'metadata'
                "data": {"ID": 1, "Price": 15.375, "Title": "UK Package"}
            },
            {  # metafilter
                "operation": (
                    "insert",
                    "update",
                ),
                "record-type": "data",
                "schema-name": "tigrillo",
            },
            None,  # record
            None,  # metadata
        ),
        (
            {},  # dms_record, empty
            {  # metafilter
                "operation": (
                    "insert",
                    "update",
                ),
                "record-type": "data",
                "schema-name": "tigrillo",
            },
            None,  # record
            None,  # metadata
        ),
        (
            {  # dms_record, filter unmatch
                "data": {"ID": 1, "Price": 15.375, "Title": "UK Package"},
                "metadata": {
                    "record-type": "data",
                    "operation": "insert",
                    "partition-key-type": "schema-table",
                    "schema-name": "another-database",
                },
            },
            {  # metafilter
                "operation": (
                    "insert",
                    "update",
                ),
                "record-type": "data",
                "schema-name": "tigrillo",
            },
            None,  # record
            {  # metadata
                "record-type": "data",
                "operation": "insert",
                "partition-key-type": "schema-table",
                "schema-name": "another-database",
            },
        ),
    ],
)
def test_dms_parse_event(
    dynamodb_table, dms_record, metafilter, expected_record, expected_metadata
):
    """Test _dms_parse_event()"""
    package_syncer = get_sqstodynamodb(dynamodb_table)
    package_syncer._dms_parse_event(
        dms_record, metafilter
    ) == expected_record, expected_metadata


@pytest.mark.parametrize(
    "input, output",
    [
        ("2020-01-30T12:30:45Z", "2020-01-30 12:30:45"),
        ("2020-01-30 12:30:45", "2020-01-30 12:30:45"),
        ("2020-01-30T12:30:45", "2020-01-30 12:30:45"),
        ("2020-01-30T12:30:45+02:00", "2020-01-30 10:30:45"),
        ("2020-01-30T12:30:45-05:00", "2020-01-30 17:30:45"),
        ("something-else", "something-else"),
        (123, 123),
        ("TZ", "TZ"),
        ("2020", "2020"),
        ("", ""),
        ("\\N", "\\N"),
        (3.1415, 3.1415),
        ("3.1415", "3.1415"),
    ],
)
def test_change_datetime_format(dynamodb_table, input, output):
    """Test _change_date_and_time_format"""
    ps = get_sqstodynamodb(dynamodb_table)
    assert ps._change_datetime_format(input) == output


@pytest.mark.parametrize(
    "input",
    [
        "9999-99-99T99:99:99Z",
        "2020-13-14T12:30:12",
        "2020-10-32T12:30:12",
        "0000-00-00T00:00:00",
    ],
)
def test_change_datetime_format_invalid_dates(dynamodb_table, input):
    """
    Test _change_date_and_time_format with bad dates and make sure we get ValueError
    """
    ps = get_sqstodynamodb(dynamodb_table)
    with pytest.raises(ValueError):
        ps._change_datetime_format(input)
