# Introduction
This is a test project.

```
MySQL         -> DMS             -> SQS     -> Lambda           -> DynamoDB
(data source)    (reads changes)    (queue)    (this function)     (package-all)
```

SQS2DynamoDB lambda function consumes SQS messages and produces DynamoDB records.

Package and MNO are two MySQL tables. We receive insert, update and delete operations to these tables as messages to an Amazon SQS queue. SQS messages has `data` and `metadata` parts where `data` is row data and `metadata` is metadata such as the operation type, the table name etc.

SQS2DynamoDB lambda function consumes these messages and creates items for `Package` and `MNO` tables
in a DynamoDB table called  `package-all`.

`package-all` table has (`ItemHash`, `ItemRange`) generic attributes for hash and range keys and they together make the primary key.

The following message is a `insert` operation for `Package` table for row with ID = 8090 for exmaple:
```JSON
{
    "data": {
        "ID": "8090",
        "Activated": "2013-09-09 15:11:24",
        "MNOID": "894453",
        "Title": "10MB/Month Zone 1 (SMS+VPN+CSD) Pooled",
        "Description": "10MB/Month Zone 1  (SMS+VPN+CSD) ONLY Data Number Pooled - no longer activate only pre-provision on package",
        "PackageCurrencyID": "GBP",
        "SIMPrice": "3.00",
        "NoChargePeriods": "0",
        "Price": "8.00",
        "Period": "1",
        "MinNoOfPeriods": "12",
        "BillingDayOfMonth": "1",
        "PackageCategoryID": "5",
        "OutOfService": "\\N",
        "State": "CR8D",
        "DefaultMCC": "234",
        "AutoProvision": "no",
        "Flags": "\\N",
        "CreatedDate": "2016-01-21T11:48:07Z",
        "ModifiedDate": "2018-10-18T09:42:57Z",
    },
    "metadata": {
        "timestamp": "2020-09-30T10: 00: 44.316222Z",
        "record-type": "data",
        "operation": "insert",
        "partition-key-type": "schema-table",
        "schema-name": "tigrillo",
        "table-name": "Package",
        "transaction-id": 8224883005967,
    }
}
```

SQSToDynamoDB lambda function should create the following DynamoDB item for this Package record:

```JSON
{
    "ItemHash": "Package|ID:8090",
    "ItemRange": "Activated:2013-09-09 15:11:24",
    "ID": "8090",
    "Activated": "2013-09-09 15:11:24",
    "Title": "10MB/Month Zone 1 (SMS+VPN+CSD) Pooled",
    "Description": "10MB/Month Zone 1  (SMS+VPN+CSD) ONLY Data Number Pooled - no longer activate only pre-provision on package",
    "PackageCurrencyID": "GBP",
    "SIMPrice": "3.00",
    "NoChargePeriods": "0",
    "Price": "8.00",
    "Period": "1",
    "MinNoOfPeriods": "12",
    "BillingDayOfMonth": "1",
    "PackageCategoryID": "5",
    "OutOfService": "\\N",
    "State": "CR8D",
    "DefaultMCC": "234",
    "AutoProvision": "no",
    "Flags": "\\N",
    "CreatedDate": "2016-01-21 11:48:07",
    "ModifiedDate": "2018-10-18 09:42:57",
}
```

Note that, for Package records, ItemHash is "Package|ID:<ID>" and ItemRange is "Activated:<Activated>".
For `MNO` table, the ItemHash is always set as "MNO" and ItemRange is "ID:<ID>".
All the other attributes are stored as is.

# High Level Requirements
- If a message for a new `Package` or `MNO` is received from SQS, an item should be created in DynamoDB table and the item should have all attributes received as well as ItemHash and ItemRange key attributes.
- If a message for an updated `Package` or `MNO` is received from sqs, the relevant Package or MNO item in DynamoDB should be updated. This could be a Price change for example.
- If a message for an deleted data is received, it should be ignored.
- If an insert or update for any other tables is received, it should be ignored.

